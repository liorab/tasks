
@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@if(Request::is('tasks'))
<a href="{{action('TaskController@myTasks')}}">my tasks</a>
@else
<a href="{{route('tasks.index')}}">show all tasks</a>
@endif


<h1>This is your tasks list</h1>
<table >
 
@foreach($tasks as $task)
<tr>  
    <td>
    @if ($task->status)

         Done!

      @else
         @cannot('user')
           <button style="text-decoration: underline" id ="{{$task->id}}" value="0"> Mark as done</button>
           @endcannot
      @endif
    </td>
    <td>
        {{$task->title}}
    </td>
    
    <td><a href = "{{route('tasks.edit', $task->id)}}"> edit </a></td> 
    
    @cannot('user')
    <td> <form method = 'post' action = "{{action('TaskController@destroy', $task->id)}}" >
@csrf
@method ('DELETE')
<div class = "form-group">
<input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>
</form></td>
@endcannot
    </tr>
    @endforeach
</table> 
<a href = "{{route('tasks.create')}}"> Create a new Task</a>
@cannot('user')
<script>
    $(document).ready(function(){
        $("button").click(function(event){
            $.ajax({
            url:  "{{url('tasks')}}" + '/' + event.target.id,
            dataType: 'json',
            type:  'put',
            contentType: 'application/json',
            data: JSON.stringify({'status':(event.target.value-1)*(-1), _token:'{{csrf_token()}}'}),
            processData: false,
            success: function( data){
                console.log(JSON.stringify( data ));
            },
            error: function(errorThrown ){
                console.log( errorThrown );
            }
            });               
        location.reload();
        });
    });
</script>
@endcannot
@endsection